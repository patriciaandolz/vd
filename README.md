Repositori amb els **scripts en R** per processar les dades del PAX:
* 1.clean_data.R
* 2.natural_language_processing.R
    

**Fitxers obtinguts** de l'execució del codi R anterior:
* 1.data_filtered.csv
* 1.data_groups.csv
* 1.data_topics.csv
* 2.agreements_interesting_words.csv --> comprimit en un ZIP per superar els 10MB permesos


**Codi en python** per a la generació dels wordclouds:
* 3.wordclouds.py
    
