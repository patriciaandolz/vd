
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
from PIL import Image
from wordcloud import WordCloud
from os import path
import pandas as pd
import numpy as np
import operator
import random 


names = ['AgreementId', 'GeneralType', 'Word', 'Freq']
df = pd.read_csv('agreements_interesting_words.csv')
df.head() 


circle = np.array(Image.open("cercle.png"))
def my_tf_color_func(word, **kwargs):
     return "white"



verb = df[df['GeneralType'] == 'Verb']
verb = verb.drop(columns=['AgreementId'])
verb.head()
v = verb.groupby('Word').sum()
data_dict_v = v.to_dict()    
wc_v = WordCloud(font_path='arial', mask=circle, margin=10, prefer_horizontal=1, max_words=50,background_color="rgba(255, 255, 255, 0)", mode="RGBA", color_func=my_tf_color_func).generate_from_frequencies(dict(data_dict_v['Freq']))
plt.imshow(wc_v)
plt.axis("off")
plt.show()
wc_v.to_file('verbs.png')



noun = df[df['GeneralType'] == 'Noun']
noun = noun.drop(columns=['AgreementId'])
noun.head()
n = noun.groupby('Word').sum()
data_dict_n = n.to_dict() 
wc_n = WordCloud(font_path='arial', mask=circle, margin=10, prefer_horizontal=1, max_words=50,background_color="rgba(255, 255, 255, 0)", mode="RGBA", color_func=my_tf_color_func).generate_from_frequencies(dict(data_dict_n['Freq']))
plt.imshow(wc_n)
plt.axis("off")
plt.show()
wc_n.to_file('nouns.png')



adjective = df[df['GeneralType'] == 'Adjective']
adjective = adjective.drop(columns=['AgreementId'])
adjective.head()
j = adjective.groupby('Word').sum()
data_dict_j = j.to_dict() 
wc_j = WordCloud(font_path='arial', mask=circle, margin=10, prefer_horizontal=1, max_words=50,background_color="rgba(255, 255, 255, 0)", mode="RGBA", color_func=my_tf_color_func).generate_from_frequencies(dict(data_dict_j['Freq']))
plt.imshow(wc_j)
plt.axis("off")
plt.show()
wc_j.to_file('adjectives.png')



adverbs = df[df['GeneralType'] == 'Adverb']
adverbs = adverbs.drop(columns=['AgreementId'])
adverbs.head()
a = adverbs.groupby('Word').sum()
data_dict_a = a.to_dict() 
wc_a = WordCloud(font_path='arial', mask=circle, margin=10, prefer_horizontal=1, max_words=25,background_color="rgba(255, 255, 255, 0)", mode="RGBA", color_func=my_tf_color_func).generate_from_frequencies(dict(data_dict_a['Freq']))
plt.imshow(wc_a)
plt.axis("off")
plt.show()
wc_a.to_file('adverbs.png')



others = df[df['GeneralType'] == 'Other']
others = others.drop(columns=['AgreementId'])
others.head()
o = others.groupby('Word').sum()
data_dict_o = o.to_dict() 
wc_o = WordCloud(font_path='arial', mask=circle, margin=10, prefer_horizontal=1, max_words=200,background_color="rgba(255, 255, 255, 0)", mode="RGBA", color_func=my_tf_color_func).generate_from_frequencies(dict(data_dict_o['Freq']))
plt.imshow(wc_o)
plt.axis("off")
plt.show()
wc_o.to_file('others.png')
